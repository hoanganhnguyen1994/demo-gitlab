# demo-runner

```

[[runners]]
  name = "docker in docker new"
  url = "https://gitlab.com/"
  token = "VRtU"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:19.03.12"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = [ "/cache"]
    shm_size = 0

```


----

### setup gitlab runner
```shell

gitlab-runner.exe register --non-interactive --executor "docker" --docker-image maven:3-jdk-8  --url "https://gitlab.com/"  --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to build java application"  --tag-list "maven-in-docker" --run-untagged="false"  --locked="false" --access-level="not_protected"
gitlab-runner.exe register --non-interactive --executor "docker" --docker-image maven:3-jdk-8  --url "https://gitlab.com/"  --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to build java application"  --tag-list "maven-in-docker" --run-untagged="false"  --locked="false" --access-level="not_protected"
gitlab-runner.exe register --non-interactive --executor "docker" --docker-image maven:3-jdk-8  --url "https://gitlab.com/"  --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to build java application"  --tag-list "maven-in-docker" --run-untagged="false"  --locked="false" --access-level="not_protected"

gitlab-runner.exe register --non-interactive --executor "docker" --docker-image node:alpine --url "https://gitlab.com/" --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to build javascript application" --tag-list "node-in-docker" --run-untagged="false" --locked="false" --access-level="not_protected"
gitlab-runner.exe register --non-interactive --executor "docker" --docker-image node:alpine --url "https://gitlab.com/" --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to build javascript application" --tag-list "node-in-docker" --run-untagged="false" --locked="false" --access-level="not_protected"

gitlab-runner.exe register --non-interactive --executor "docker" --docker-image hcglab/kubectl:latest --url "https://gitlab.com/" --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to deploy" --tag-list "deploy-locally" --run-untagged="false" --locked="false" --access-level="not_protected"
gitlab-runner.exe register --non-interactive --executor "docker" --docker-image hcglab/kubectl:latest --url "https://gitlab.com/" --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to deploy" --tag-list "deploy-locally" --run-untagged="false" --locked="false" --access-level="not_protected"
gitlab-runner.exe register --non-interactive --executor "docker" --docker-image hcglab/kubectl:latest --url "https://gitlab.com/" --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to deploy" --tag-list "deploy-locally" --run-untagged="false" --locked="false" --access-level="not_protected"

gitlab-runner.exe register -n --executor "docker" --docker-image docker:19.03.12 --url "https://gitlab.com/" --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to publish image" --tag-list "docker-in-docker" --run-untagged="false" --locked="false" --access-level="not_protected" --docker-volumes /var/run/docker.sock:/var/run/docker.sock
gitlab-runner.exe register -n --executor "docker" --docker-image docker:19.03.12 --url "https://gitlab.com/" --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to publish image" --tag-list "docker-in-docker" --run-untagged="false" --locked="false" --access-level="not_protected" --docker-volumes /var/run/docker.sock:/var/run/docker.sock
gitlab-runner.exe register -n --executor "docker" --docker-image docker:19.03.12 --url "https://gitlab.com/" --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to publish image" --tag-list "docker-in-docker" --run-untagged="false" --locked="false" --access-level="not_protected" --docker-volumes /var/run/docker.sock:/var/run/docker.sock

gitlab-runner.exe register -n --executor "docker" --docker-image ruby:2.5 --url "https://gitlab.com/" --registration-token "uXhXRR5363CFauzC92TA " --description "Runner use to publish image" --tag-list "docker-in-docker" --run-untagged="false" --locked="false" --access-level="not_protected" --docker-volumes /var/run/docker.sock:/var/run/docker.sock

```

feat: allow provided 2


update 1
update 3
update 4

update 5
update 6
update 7

update 8
update 9

update 10 test merge default
update 10 test merge default
update 10 test merge default

update 10 test merge default
update 12 test merge default
update 12 test merge default
update 13 test merge default
feat: feature 1

update 2
